#!/bin/bash
(
  if [ $# != 1 ]; then
    echo "One argument is required -> Indicate the type of change (--minor, --major)"
    return 0
  fi

  if [ -f .env ]; then
    echo "Loading ENV variables..."
    export $(echo $(cat .env | sed 's/#.*//g' | xargs) | envsubst)
  fi

  echo "Generating config file for kaniko..."
  echo "{\"auths\":{\"https://denim-registry.idener.es/v2/\":{\"username\":\"admin\",\"password\":\"$PASSWORD_DOCKER_REGISTRY\"}}}" >config.json

  DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
  typeChange=$1

  pushd "${DIR}" || exit
  echo
  echo "Building image from the given Dockerfile using Kaniko"
  echo

  if [ -d "${DIR}/logs" ]; then
    echo "Logs folder exists"
  else
    mkdir -p "${DIR}/logs"
    echo "Creating logs folder"
  fi

  if [ -d "${DIR}/images" ]; then
    echo "Directory "${DIR}/images"   exists."
    for d in images/*/; do
      prefix="images"
      NEW_DIR=$(basename "$d" | sed -e "s/^$prefix//")
      IMAGE_NAME=${PWD##*/}-$NEW_DIR

      TAG=$(curl -X GET -u admin:$PASSWORD_DOCKER_REGISTRY https://denim-registry.idener.es/v2/$IMAGE_NAME/tags/list | jq -r '.tags' | tr -d [],latest)

      LATEST_TAG=0
      for elem in $TAG; do
        if (($(echo "$LATEST_TAG < ${elem:1:-1} " | bc))); then
          LATEST_TAG=${elem:1:-1}
        fi
      done
      if [[ $1 == "minor" ]]; then
        INCREMENT=0.1
        UPDATE_TAG=$(echo $LATEST_TAG+$INCREMENT | bc)
        if (($(echo "$UPDATE_TAG < 1" | bc))); then
          UPDATE_TAG=0$UPDATE_TAG
        fi
      fi
      if [[ $1 == "major" ]]; then
        INCREMENT=1.0
        SUM=$LATEST_TAG+$INCREMENT
        UPDATE_TAG=$(echo $LATEST_TAG+$INCREMENT | bc)
        UPDATE_TAG=${UPDATE_TAG/\.*/}.0
      fi

      echo "Current tag of $IMAGE_NAME is $LATEST_TAG"
      echo "New tag of $IMAGE_NAME will be $UPDATE_TAG"

      docker run -v $(pwd)/config.json:/kaniko/.docker/config.json:ro -v $(pwd):/workspace gcr.io/kaniko-project/executor:latest --destination "denim-registry.idener.es/$IMAGE_NAME:"$UPDATE_TAG"" --context=$(pwd)
    done
  else
    echo "Error: Directory "${DIR}/images"  does not exists."

    IMAGE_NAME=${PWD##*/}

    TAG=$(curl -X GET -u admin:$PASSWORD_DOCKER_REGISTRY https://denim-registry.idener.es/v2/$IMAGE_NAME/tags/list | jq -r '.tags' | tr -d [],latest)

    LATEST_TAG=0
    for elem in $TAG; do
      if (($(echo "$LATEST_TAG < ${elem:1:-1} " | bc))); then
        LATEST_TAG=${elem:1:-1}
      fi
    done
    if [[ $1 == "minor" ]]; then
      INCREMENT=0.1
      UPDATE_TAG=$(echo $LATEST_TAG+$INCREMENT | bc)
      if (($(echo "$UPDATE_TAG < 1" | bc))); then
        UPDATE_TAG=0$UPDATE_TAG
      fi
    fi
    if [[ $1 == "major" ]]; then
      INCREMENT=1.0
      SUM=$LATEST_TAG+$INCREMENT
      UPDATE_TAG=$(echo $LATEST_TAG+$INCREMENT | bc)
      UPDATE_TAG=${UPDATE_TAG/\.*/}.0
    fi

    echo "Current tag of $IMAGE_NAME is $LATEST_TAG"
    echo "New tag of $IMAGE_NAME will be $UPDATE_TAG"

    docker run -v $(pwd)/config.json:/kaniko/.docker/config.json:ro -v $(pwd):/workspace gcr.io/kaniko-project/executor:latest --destination "denim-registry.idener.es/$IMAGE_NAME:"$UPDATE_TAG"" --context=$(pwd)

  fi

) 2>&1 | tee logs/results_$(date '+%F_%H:%M:%S').log
