# Build images with Kaniko
Kaniko is a tool to build container images from a Dockerfile, inside a container or Kubernetes cluster. Kaniko doesn't depend on a Docker daemon and executes each command within a Dockerfile completely in userspace. This enables building container images in environments that can't easily or securely run a Docker daemon, such as a standard Kubernetes cluster. Kaniko is meant to be run as an image: gcr.io/kaniko-project/executor

In order to accomplish this, a script known as run.sh is provided. There are two different methods to structure the repository, both of which will allow it to be successfully executed:

* If there are only image per repository the Dockerfile should be created in the root of the repository. In this case, the name of the pushed image will be the name of the repository.
* If there are more than one image per repository, each image must be created inside the "images" folder within the desired name for the custom image as name of the folder. Following this proceture, the name of the image will be the name of the repository plus the name of the subfolder inside "images" directory. 

To build the images using Kaniko, follow the steps outlined in the next points:

1. Create a .env file with one variable -> PASSWORD_DOCKER_REGISTRY using as value the Docker Registry password stored in Bitwarden:
    ```shell
    PASSWORD_DOCKER_REGISTRY="TO BE FILLED"
    ```
 
2. Build the image by running a custom shell script indicating if it is a minor or major change as an argument:
    ```shell
    source run.sh minor
    ```
    * If the change is "minor", the increment of the tag will be 0.1 respect to the previous one (e.g., 0.1 -> 0.2)
    * If the change is "major", the next integer with respect to the current number will be the tag (e.g., 0.4 -> 1.0) 

3. To test if the image has been properly pushed, simply execute the following commands: 

    ```shell
    # Check if the image exists in the repository
    curl -X GET -u admin:$PASSWORD_DOCKER_REGISTRY https://denim-registry.idener.es/v2/_catalog | grep "NAME OF IMAGE TO TEST"
    # Check if the new tag has been pushed to the registry
    curl -X GET -u admin:$PASSWORD_DOCKER_REGISTRY https://denim-registry.idener.es/v2/"NAME OF IMAGE TO TES/tags/list |
    ```

